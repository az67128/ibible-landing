export const IS_TEST = /(localhost|192\.168)/.test(
    window.location.href
  );

const sendMetrik = (name, payload) => {
    const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
    const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
    ym(86428733, "reachGoal", `${name}`, payload);
  };

  export default sendMetrik