import { elementScrollIntoView } from 'seamless-scroll-polyfill'

import { writable, derived } from 'svelte/store'
import items from './items'

const searchParams = new URLSearchParams(window.location.search)
const initialSeries = Number(searchParams.get('episode'))
export const activeSeriesIndex = writable(
  initialSeries && !Number.isNaN(initialSeries) && initialSeries <= items.length ? initialSeries - 1 : 0,
)
export const isMobilePlayerVisible = writable(false)
export const toggleMobilePlayerVisible = () => {
  isMobilePlayerVisible.update((state) => {
    const newState = !state
    if (newState) {
      window?.azure?.play()
    } else {
      window?.azure?.pause()
    }
    return newState
  })
}

export const setSeries = (i) => {
  activeSeriesIndex.set(i)
  if (window.innerWidth > 650) {
    elementScrollIntoView(document.querySelector('#anchor'), { behavior: 'smooth' })
  } else {
    setTimeout(() => {
      toggleMobilePlayerVisible()
    })
  }
}

export const nextSeries = () => {
  activeSeriesIndex.update((index) => {
    if (index === items.length - 1) {
      return index
    }
    window.azure.autoplay(true)
    return index + 1
  })
}

export const activeSeries = derived(activeSeriesIndex, ($activeSeriesIndex) => {
  return items[$activeSeriesIndex]
})

activeSeries.subscribe((store) => {
  if (window.azure) {
    window.azure.src([
      {
        src: store.url,
        type: 'application/vnd.ms-sstr+xml',
      },
    ])
  }
})

export const isPolicyOpen = writable(false)

export function preventDefault(e) {
  e.preventDefault()
}